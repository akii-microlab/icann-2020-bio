# ICANN 2020 Bio Paper submission
Code repository for "Tumor Characterization using Unsupervised Learning of Mathematical Relations within Breast Cancer Data" by Cristian Axenie and Daria Kurz submitted at ENNS ICANN2020 (15th – 18th September 2020).

# Abstract 
Despite the variety of imaging, genetic and histopathological data used to assess tumors, there is still an unmet need for patient-specific tumor growth profile extraction and tumor volume prediction, for use in surgery planning. Models of tumor growth predict tumor size based on measurements made in histological images of individual patients’ tumors compared to diagnostic imaging. Typically, such models require tumor biology-dependent parametrization, which hardly generalizes to cope with tumor variability among patients. In addition, the histopathology specimens datasets are limited in size, owing to the restricted or single-time measurements. In this work, we address the shortcomings that incomplete biological specifications, the inter-patient variability of tumors, and the limited size of the data bring to mechanistic tumor growth models and introduce a machine learning model capable of characterizing a tumor, namely its growth pattern, phenotypical transitions, and volume. The model learns without supervision, from different types of breast cancer data the underlying mathematical relations describing tumor growth curves more accurate than three state-of-the-art models on three publicly available clinical breast cancer datasets, being versatile among breast cancer types. Moreover, the model can also, without modification, learn the mathematical relations among, for instance, histopathological and morphological parameters of the tumor and together with the growth curve capture the (phenotypical) growth transitions of the tumor from a small amount of data. Finally, given the tumor growth curve and its transitions, our model can learn the relation among tumor proliferation-to-apoptosis ratio, tumor radius, and tumor nutrient diffusion length to estimate tumor volume, which can be readily incorporated within current clinical practice, for surgery planning. We demonstrate the broad capabilities of our model through a series of experiments on publicly available clinical datasets.

**Codebase:**

datasets - the experimental datasets (csv files) and their source, each in separate directories
models   - codebase to run and reproduce the experiments

**Directory structure:**

model/.

* create_init_network.m       - init networks (SOM + HL)
* error_std.m                 - error std calculation function
* tumor_estimator_core.m      - main script to run the system
* model_rmse.m                - RMSE calculation function 
* model_sse.m                 - SSE calculation function
* parametrize_learning_law.m  - function to parametrize learning
* present_tuning_curves.m     - function to visualize SOM tuning curves
* randnum_gen.m               - weight initialization function
* tumor_growth_model_fit.m    - function implementing ODE models
* tumor_growth_models_eval.m  - main evaluation on runtime
* visualize_results.m         - visualize output and internals
* visualize_runtime.m         - visualize runtime



**Usage:**

* model/tumor_estimator_core.m - main function that runs the system and generates the runtime output file (mat file)
* model/tumor_growth_models_eval.m - evaluation and plotting function reading the runtime output file

